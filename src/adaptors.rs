//!Future adaptors to transform some builtin types into `Future`

use core::task;
use core::pin::Pin;
use core::future::Future;

///Wrapper over Option that implements `Future`
///
///If there is `Some(T)` then it is polled.
///Otherwise it returns `None` immediately.
///
///## Usage:
///
///```rust,no_run
///use cute_async::IntoFuture;
///
///use core::task;
///use core::pin::Pin;
///use core::future::Future;
///
///pub struct MyFuture;
///
///impl Future for MyFuture {
///    type Output = ();
///
///    fn poll(self: Pin<&mut Self>, ctx: &mut task::Context<'_>) -> task::Poll<Self::Output> {
///        task::Poll::Ready(())
///    }
///}
///
///let fut_maybe = if true {
///    Some(MyFuture)
///} else {
///    None
///};
///
///let mut future = fut_maybe.into_future();
///```
pub struct OptionFut<F> {
    inner: Option<F>,
}

impl<F> From<Option<F>> for OptionFut<F> {
    fn from(inner: Option<F>) -> Self {
        Self {
            inner
        }
    }
}

impl<F: Unpin> Unpin for OptionFut<F> {}

impl<F: Future> Future for OptionFut<F> {
    type Output = Option<F::Output>;

    fn poll(self: Pin<&mut Self>, ctx: &mut task::Context<'_>) -> task::Poll<Self::Output> {
        let this = unsafe {
            self.get_unchecked_mut()
        };

        match this.inner.as_mut() {
            Some(fut) => Future::poll(unsafe { Pin::new_unchecked(fut) }, ctx).map(|res| Some(res)),
            None => task::Poll::Ready(None)
        }
    }
}

impl<F: Future> crate::fut::IntoFuture for Option<F> {
    type Output = OptionFut<F>;

    fn into_future(self) -> Self::Output {
        self.into()
    }
}

///Wrapper over Result that implements `Future`
///
///Depending on variant, it polls underlying future and returns `Output` in corresponding variant.
///
///## Usage:
///
///```rust,no_run
///use cute_async::IntoFuture;
///
///use core::task;
///use core::pin::Pin;
///use core::future::Future;
///
///pub struct MyFuture;
///
///impl Future for MyFuture {
///    type Output = ();
///
///    fn poll(self: Pin<&mut Self>, ctx: &mut task::Context<'_>) -> task::Poll<Self::Output> {
///        task::Poll::Ready(())
///    }
///}
///
///let result = if true {
///    Ok(MyFuture)
///} else {
///    Err(MyFuture)
///};
///
///let mut future = result.into_future();
///```
pub struct ResultFut<T, E> {
    inner: Result<T, E>
}

impl<T, E> From<Result<T, E>> for ResultFut<T, E> {
    fn from(inner: Result<T, E>) -> Self {
        Self {
            inner
        }
    }
}

impl<T: Unpin, E: Unpin> Unpin for ResultFut<T, E> {}

impl<T: Future, E: Future> Future for ResultFut<T, E> {
    type Output = Result<T::Output, E::Output>;

    fn poll(self: Pin<&mut Self>, ctx: &mut task::Context<'_>) -> task::Poll<Self::Output> {
        let this = unsafe {
            self.get_unchecked_mut()
        };

        match this.inner.as_mut() {
            Ok(ok) => Future::poll(unsafe { Pin::new_unchecked(ok) }, ctx).map(|res| Ok(res)),
            Err(err) => Future::poll(unsafe { Pin::new_unchecked(err) }, ctx).map(|res| Err(res)),
        }
    }
}

impl<T: Future, E: Future> crate::fut::IntoFuture for Result<T, E> {
    type Output = ResultFut<T, E>;

    fn into_future(self) -> Self::Output {
        self.into()
    }
}
