//!Future related utilities

use core::task;
use core::future::Future;
use core::pin::Pin;

///Trait describing tranformation into future
pub trait IntoFuture {
    ///Resulting Future.
    type Output: Future;

    ///Transforms self into Future
    fn into_future(self) -> Self::Output;
}

#[derive(Debug)]
///Either variant.
pub enum Either<A, B> {
    ///Left
    Left(A),
    ///Right
    Right(B),
}

impl<A, B> Either<A, B> {
    ///Returns whether it is Left variant or not.
    pub fn is_left(&self) -> bool {
        match self {
            Either::Left(_) => true,
            Either::Right(_) => false,
        }
    }
}

impl<A> Either<A, A> {
    ///Consumes self, returning underlying value
    ///
    ///Works only if they are of the same type.
    pub fn into(self) -> A {
        match self {
            Either::Left(res) => res,
            Either::Right(res) => res,
        }
    }
}

impl<A: Unpin, B: Unpin> Unpin for Either<A, B> {}

impl<A: Unpin, B: Unpin> Future for Either<A, B> where A: Future, B: Future<Output = A::Output> {
    type Output = A::Output;

    fn poll(mut self: Pin<&mut Self>, ctx: &mut task::Context<'_>) -> task::Poll<Self::Output> {
        match *self {
            Either::Left(ref mut left) => Future::poll(Pin::new(left), ctx),
            Either::Right(ref mut right) => Future::poll(Pin::new(right), ctx),
        }
    }
}

#[derive(Debug)]
///The result of Pair.
///
///First argument is Output of completed future
///Second is unfinished Future.
///
///Can be polled again to finish the second Future and return tuple of results
pub struct UnfinishedPair<O, F> {
    inner: Option<(O, F)>
}

impl<O, F: Future> UnfinishedPair<O, F> {
    #[inline(always)]
    ///Creates new instance.
    ///
    ///Order is enforced via type arguments
    pub fn new(output: O, fut: F) -> Self {
        Self {
            inner: Some((output, fut))
        }
    }

    #[inline(always)]
    ///Consumes self returning underlying parts
    pub fn into_parts(self) -> (O, F) {
        self.inner.unwrap()
    }
}

impl<O: Unpin, F: Unpin> Unpin for UnfinishedPair<O, F> {}

impl<O: Unpin, F: Unpin> Future for UnfinishedPair<O, F> where F: Future {
    type Output = (O, F::Output);

    fn poll(mut self: Pin<&mut Self>, cx: &mut task::Context<'_>) -> task::Poll<Self::Output> {
        let (_, ref mut fut) = match self.inner.as_mut() {
            Some(value) => value,
            None => unreach!()
        };

        match Pin::new(fut).poll(cx) {
            task::Poll::Ready(res) => {
                let left = match self.inner.take() {
                    Some((left, _)) => left,
                    None => unreach!()
                };
                task::Poll::Ready((left, res))
            }
            task::Poll::Pending => task::Poll::Pending,
        }
    }
}

#[derive(Debug)]
///Pair of futures that are being processed together.
///
///First goes left, then right.
///
///Resulting `Either` will contain output of future, alongside not completed future.
pub struct Pair<A, B> {
    inner: Option<(A, B)>,
}

impl<A, B> Pair<A, B> {
    #[inline(always)]
    ///Creates new instance
    pub fn new(left: A, right: B) -> Self {
        Self {
            inner: Some((left, right)),
        }
    }

    #[inline(always)]
    ///Consumes self returning underlying parts
    pub fn into_parts(self) -> (A, B) {
        self.inner.unwrap()
    }
}

impl<A: Unpin, B: Unpin> Unpin for Pair<A, B> {}

impl<A: Unpin, B: Unpin> Future for Pair<A, B> where A: Future, B: Future {
    type Output = Either<UnfinishedPair<A::Output, B>, UnfinishedPair<B::Output, A>>;

    fn poll(mut self: Pin<&mut Self>, cx: &mut task::Context<'_>) -> task::Poll<Self::Output> {
        let (ref mut left, ref mut right) = match self.inner.as_mut() {
            Some(value) => value,
            None => unreach!()
        };

        match Pin::new(left).poll(cx) {
            task::Poll::Ready(res) => {
                let right = match self.inner.take() {
                    Some((_, right)) => right,
                    None => unreach!()
                };
                task::Poll::Ready(Either::Left(UnfinishedPair::new(res, right)))
            },
            task::Poll::Pending => match Pin::new(right).poll(cx) {
                task::Poll::Ready(res) => {
                    let left = match self.inner.take() {
                        Some((left, _)) => left,
                        None => unreach!(),
                    };

                    task::Poll::Ready(Either::Right(UnfinishedPair::new(res, left)))
                },
                task::Poll::Pending => task::Poll::Pending,
            }
        }
    }
}
