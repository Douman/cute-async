//! Personal collection of async utilities
#![warn(missing_docs)]
#![cfg_attr(feature = "cargo-clippy", allow(clippy::style))]

///Await future in async context.
///
///Because `.await` is retarded.
///
///```rust
///async fn do_async() {
///}
///
///async fn my_main() {
///    cute_async::matsu!(do_async());
///}
///
///```
#[macro_export]
macro_rules! matsu {
    ($exp:expr) => {
        ($exp).await
    }
}

///Unreachable optimization hint.
///
///In debug mod it panics using `unreachable!` macro
///But hitting this macro in release mode  would result in UB.
#[macro_export]
macro_rules! unreach {
    () => ({
        #[cfg(not(debug_assertions))]
        unsafe {
            std::hint::unreachable_unchecked();
        }
        #[cfg(debug_assertions)]
        {
            unreachable!()
        }
    })
}

///Gets `Pin` out of value
///
///## Usage:
///
///```rust,no_run
///use cute_async::AsPin;
///
///use core::task;
///use core::pin::Pin;
///use core::future::Future;
///
///pub struct MyFuture<T>(T);
///
///impl<T: AsPin + Unpin + Future> Future for MyFuture<T> {
///    type Output = T::Output;
///
///    fn poll(mut self: Pin<&mut Self>, ctx: &mut task::Context<'_>) -> task::Poll<Self::Output> {
///        let inner = self.0.as_pin();
///        Future::poll(inner, ctx)
///    }
///}
///```

pub trait AsPin: core::ops::Deref {
    ///Gets `Pin` out of self.
    fn as_pin(&mut self) -> core::pin::Pin<&'_ mut Self>;
}

impl<T: core::ops::Deref> AsPin for T {
    #[inline(always)]
    fn as_pin(&mut self) -> core::pin::Pin<&'_ mut Self> {
        unsafe {
            core::pin::Pin::new_unchecked(self)
        }
    }
}

pub mod fut;
pub use fut::*;
pub mod adaptors;
pub use adaptors::*;
