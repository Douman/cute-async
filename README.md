# cute-async

[![Build](https://gitlab.com/Douman/cute-async/badges/master/build.svg)](https://gitlab.com/Douman/cute-async/pipelines)
[![Documentation](https://docs.rs/cute-async/badge.svg)](https://docs.rs/crate/cute-async/)

Cute async utilities.
